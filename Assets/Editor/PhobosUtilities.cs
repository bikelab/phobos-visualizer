﻿using UnityEngine;
using UnityEditor;

public class PhobosUtilities {
	[MenuItem("Phobos/To Terrain Height")]
    static void MoveGameObjectToTerrain() {
		var activeTerrain = Terrain.activeTerrain;
		foreach (var gameObject in Selection.gameObjects) {
			var activeTransform = gameObject.transform;
			activeTransform.position = new Vector3(
				activeTransform.position.x, 
				activeTerrain.SampleHeight(activeTransform.position) + activeTerrain.transform.position.y, 
				activeTransform.position.z
			);
		}
    }

	[MenuItem("Phobos/To Terrain Height",true)]
    static bool ValidateMoveGameObjectToTerrain() {
		return Selection.gameObjects.Length > 0;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class OnSelectToggle : MonoBehaviour, ISelectHandler {

	public enum Action {
		Off,
		On,
		Toggle
	}

	[SerializeField]
	Toggle toggle;

	[SerializeField]
	Action action = Action.Toggle;

	public void OnSelect(BaseEventData data) {
		switch (action) {
			case Action.Off:
				toggle.isOn = false;
				break;
			case Action.On:
				toggle.isOn = true;
				break;
			case Action.Toggle:
				toggle.isOn = !toggle.isOn;
				break;
		}
	}
}
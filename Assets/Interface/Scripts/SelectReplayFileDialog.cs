﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using UnityEngine;
using UnityEngine.UI;

public class SelectReplayFileDialog : MonoBehaviour {

	[SerializeField] Dropdown detectionDropdown;
	[SerializeField] Button loadButton;
	[SerializeField] Text noFilesText;

	public void OpenDialog() {
		RefreshFiles();
		gameObject.SetActive(true);
	}

	public void CloseDialog() {
		gameObject.SetActive(false);
	}

	public void RefreshFiles() {
        string[] options = Directory.GetFiles(".", "*.pb.cobs.gz");

        Array.Sort(options);

		detectionDropdown.ClearOptions();

		if (options.Length == 0) {
			noFilesText.gameObject.SetActive(true);
            loadButton.interactable = false;

		} else {
			noFilesText.gameObject.SetActive(false);
			detectionDropdown.AddOptions(new List<string>(options));
			loadButton.interactable = true;
		}
	}

	public string GetPath() {
		return detectionDropdown.options[detectionDropdown.value].text;
	}
}

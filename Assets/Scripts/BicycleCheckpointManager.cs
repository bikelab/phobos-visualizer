﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BicycleCheckpointManager : MonoBehaviour {

	[SerializeField] int checkpointIndex = 0;
	[SerializeField] GameObject[] checkpoints;
	[SerializeField] LightShaft lightShaft;

	void OnTriggerEnter(Collider other) {
		if (checkpoints[checkpointIndex] == other.gameObject) {
			checkpointIndex += 1;
			if (checkpointIndex >= checkpoints.Length) checkpointIndex = 0;
			lightShaft.Target = checkpoints[checkpointIndex].transform;
		}
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScene : MonoBehaviour {
	public void OnLiveButton() {
		SceneManager.LoadScene("Live");
	}

	public void OnReplayButton() {
		SceneManager.LoadScene("Replay");
	}

	public void OnTestButton() {
		SceneManager.LoadScene("Test");
	}
}

using pb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

class ReplayScene : MonoBehaviour {
    [SerializeField] BicycleStateProvider bicycleStateProvider;

    [SerializeField] Slider slider = null;

    [SerializeField] SelectReplayFileDialog selectReplayFileDialog;

    // Keep track of the dragging state of the slider. Kind of sucks that we
    // have no access to this directly.
    private bool dragging = false;

    // Taken from CHIBIOS_CFG_ST_FREQUENCY. Will be in configuration message in
    // the future.
    private float clockFrequency = 100000;

    [SerializeField] private float playSpeed = 1;

    private List<SimulationMessage> simulation_states;

    private uint min_timestamp;
    private uint max_timestamp;
    private uint cur_timestamp;

    private enum ReplaySceneState {
        Uninitialized,
        SelectingFile,
        Replaying,
    }

    private ReplaySceneState state = ReplaySceneState.Uninitialized;

    private void Transition(ReplaySceneState newState) {
        // Clean up things for current state.
        switch (state) {
            case ReplaySceneState.Uninitialized:
                // Should happen once, there is nothing to do though.
                break;
            case ReplaySceneState.SelectingFile:
                selectReplayFileDialog.CloseDialog();
                break;
            case ReplaySceneState.Replaying:
                simulation_states = null;
                break;
        }

        // Update state.
        state = newState;

        // Initialize things for new state.
        switch (state) {
            case ReplaySceneState.Uninitialized:
                // Should never happen.
                break;
            case ReplaySceneState.SelectingFile:
                selectReplayFileDialog.OpenDialog();
                break;
            case ReplaySceneState.Replaying:
                LoadSimulationMessages(selectReplayFileDialog.GetPath());
                break;
        }
    }

    public void Awake() {
        Transition(ReplaySceneState.SelectingFile);
    }

    public void LoadHandler() {
        Transition(ReplaySceneState.Replaying);
    }

    public void LoadSimulationMessages(string path) {
        simulation_states = new List<SimulationMessage>();

        using (var file = new FileStream(path, FileMode.Open))
        using (var gzip = new GZipStream(file, CompressionMode.Decompress)) {
            SerialDecoder decoder = new SerialDecoder();

            int bytesRead;

            while (true) {
                // Attempt to read at most as many bytes that still fit in the read buffer.
                // This call blocks until there are bytes available or the stream ends.
                bytesRead = gzip.Read(decoder.ReadBuffer, decoder.ReadBufferOffset, decoder.ReadBufferRemainingCapacity);
                decoder.ReadBufferOffset += bytesRead;

                // If bytes_read equals 0, the stream has ended.
                if (bytesRead == 0) break;

                foreach (var message in decoder.ProcessReadBuffer()) {
                    // Filter out messages without pose.
                    if (message.pose == null) continue;
                    simulation_states.Add(message);
                }
            }
        }

        // Sort the simulation states by the timestamp.
        simulation_states.Sort((SimulationMessage a, SimulationMessage b) => {
            if (a.pose.timestamp < b.pose.timestamp) return -1;
            if (a.pose.timestamp > b.pose.timestamp) return 1;
            return 0;
        });

        // Set the timestamps.
        min_timestamp = simulation_states.First().pose.timestamp;
        max_timestamp = simulation_states.Last().pose.timestamp;
        cur_timestamp = min_timestamp;

        // Update slider range.
        slider.minValue = min_timestamp;
        slider.maxValue = max_timestamp;
    }

    public void Update() {
        switch (state) {
            case ReplaySceneState.SelectingFile:
                if (Input.GetButtonDown("Cancel")) {
                    SceneManager.LoadScene("Main");
                    break;
                }
                break;
            case ReplaySceneState.Replaying:
                if (Input.GetButtonDown("Cancel")) {
                    Transition(ReplaySceneState.SelectingFile);
                    break;
                }

                // Update either the current timestamp or the slider value. If we are
                // dragging we want to update the current timestamp based on where we
                // are dragging the slider. If we are not dragging, we want the current
                // timestamp to advance a bit and update the value of the slider to the
                // new timestamp.
                if (dragging) {
                    // Compute the current timestamp from the current slider value. Not
                    // required anymore since we are setting the min and max on the
                    // slider but whatever.
                    cur_timestamp = (uint) Map(
                        slider.value,
                        slider.minValue, slider.maxValue,
                        min_timestamp, max_timestamp
                    );
                } else {
                    cur_timestamp += (uint) (Time.deltaTime*playSpeed*clockFrequency);

                    // Wrap the current timestamp at max value.
                    if (cur_timestamp > max_timestamp) cur_timestamp = min_timestamp;

                    slider.value = Map(
                        cur_timestamp,
                        min_timestamp, max_timestamp,
                        slider.minValue, slider.maxValue
                    );
                }

                // Find the most recent state before the current timestamp.
                SimulationMessage last_state = simulation_states.FindLast(
                    state => state.pose.timestamp <= cur_timestamp
                );

                // If we found a state, update the bike transform to it.
                if (last_state != null) {
                    bicycleStateProvider.Emit(last_state);
                }
                break;
        }
    }

    public void BeginSliderDrag() {
        dragging = true;
    }

    public void EndSliderDrag() {
        dragging = false;
    }

    private float Map(float x, float x0, float x1, float y0, float y1) {
        // Slightly less efficient than the standard formula but more accurate.
        return (y0*(x1 - x) + y1*(x - x0))/(x1 - x0);
    }
}

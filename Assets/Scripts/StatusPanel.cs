﻿using UnityEngine.UI;
using UnityEngine;
using pb;
using Units.Angles;

public class StatusPanel : MonoBehaviour {
	[SerializeField] BicycleStateProvider bicycleStateProvider;

	Text fps_value = null;
	Text mps_value = null;
	Text velocity_value = null;
	Text x_value = null;
	Text y_value = null;
	AngleDisplay roll_value = null;
	AngleDisplay pitch_value = null;
	AngleDisplay yaw_value = null;
	AngleDisplay steer_value = null;
	AngleDisplay wheel_value = null;

	void OnEnable() {
		bicycleStateProvider.OnStateChange += SetStatus;
		bicycleStateProvider.OnVelocityChange += SetVelocity;
		bicycleStateProvider.OnMessagesPerSecondChange += SetMessagesPerSecond;
	}

	void OnDisable() {
		bicycleStateProvider.OnStateChange -= SetStatus;
		bicycleStateProvider.OnVelocityChange -= SetVelocity;
		bicycleStateProvider.OnMessagesPerSecondChange -= SetMessagesPerSecond;
	}

	// Use this for initialization
	void Start () {
		fps_value = transform.Find("FPS/Value").GetComponent<Text>();
		mps_value = transform.Find("MPS/Value").GetComponent<Text>();
		velocity_value = transform.Find("Velocity/Value").GetComponent<Text>();
		x_value = transform.Find("X/Value").GetComponent<Text>();
		y_value = transform.Find("Y/Value").GetComponent<Text>();
		roll_value = transform.Find("Roll/Angle Display").GetComponent<AngleDisplay>();
		pitch_value = transform.Find("Pitch/Angle Display").GetComponent<AngleDisplay>();
		yaw_value = transform.Find("Yaw/Angle Display").GetComponent<AngleDisplay>();
		steer_value = transform.Find("Steer/Angle Display").GetComponent<AngleDisplay>();
		wheel_value = transform.Find("Wheel/Angle Display").GetComponent<AngleDisplay>();
	}

	public void SetMessagesPerSecond(float mps) {
		mps_value.text = mps.ToString("0000");
	}

	public void SetVelocity(float value) {
		velocity_value.text = value.ToString("00.0");
	}

	public void SetStatus(BicycleState state) {
		x_value.text = state.X.ToString("F2");
		y_value.text = state.Y.ToString("F2");
		roll_value.SetValue(state.Roll);
		pitch_value.SetValue(state.Pitch);
		yaw_value.SetValue(state.Yaw);
		steer_value.SetValue(state.Steer);
		wheel_value.SetValue(state.Wheel);
	}

	public void Update() {
		fps_value.text = (1/UnityEngine.Time.smoothDeltaTime).ToString("000");
	}
}

# phobos-visualizer

phobos-visualizer is a [Unity](https://unity3d.com/) project that visualizes the
state of the [phobos](https://github.com/oliverlee/phobos) bicycle simulator.

## Building

There is a [build script](Scripts/build.bash) for [Unity on
linux](https://forum.unity3d.com/threads/unity-on-linux-release-notes-and-known-issues.350256/).
There is no build script for Windows but the Linux one can be converted.
Otherwise you can build manually.

## Generation of protobuf sources

Because phobos' protobuf messages use proto2 and the original protobuf
codegenerator does not support C#, we have to
use
[protobuf-net](https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/protobuf-net/protobuf-net%20r668.zip) to
generate C# code from phobos' protobuf message specification.

Unfortunately, only a Windows executable of the tool is available. Perhaps it
can be run using [wine](https://www.winehq.org/).

Assuming you download and extract the contents to `ignore\protobuf`, issue the
following command to generate classes for your protobuf messages.

```cmd
"ignore\protobuf\ProtoGen\protogen.exe" "-i:Assets\proto\simulation.proto" "-o:Assets\Scripts\pb\Messages.cs" "-ns:pb"
```

Generation of new protobuf sources is only required when the protobuf message
definition is updated.

## Debugging

[Follow this guide](https://code.visualstudio.com/docs/runtimes/unity) to debug
unity games with Visual Studio Code.

## History

This project is a rewrite of [bikesim](https://github.com/oliverlee/bikesim).

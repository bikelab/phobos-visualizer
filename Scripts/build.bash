#!/usr/bin/env bash

set -e

unity="/opt/Unity/Editor/Unity"

project_path="$(pwd)"

executable_name="phovis"
executable_folder="Phobos Visualizer"
executable_version="0.2"

function _log {
    echo "> $@"
    "$@"
}

function _build {
    local build_command="$1"
    local build_folder="$2"
    local executable_extension="$3"

    # Ensure the build folder exists so that the log file can be written.
    _log mkdir --parents "${build_folder}"

    _log "${unity}" \
        -quit \
        -batchmode \
        -nographics \
        -silent-crashes \
        -projectPath "${project_path}" \
        -logFile "${build_folder}/build-log.txt" \
        "${build_command}" "${build_folder}/${executable_folder}/${executable_name}${executable_extension}"
}

function _gzip {
    local executable_platform="$1"
    local build_folder="$2"

    _log tar \
        --create \
        --gzip \
        --directory "${build_folder}" \
        "${executable_folder}" \
        --file "${build_folder}/${executable_name}-v${executable_version}-${executable_platform}.tar.gz"
}

function _zip {
    local executable_platform="$1"
    local build_folder="$2"

    (
      cd "${build_folder}"
      _log zip \
           -q \
           -r \
           "${executable_name}-v${executable_version}-${executable_platform}.zip" \
           "${executable_folder}"
      # -q: quiet
      # -r: recursive
    )
}

function _do_linux_32 {
    _build "-buildLinux32Player" "Build/Linux32" ""
    _gzip "linux-32" "Build/Linux32"
}

function _do_linux_64 {
    _build "-buildLinux64Player" "Build/Linux64" ""
    _gzip "linux-64" "Build/Linux64"
}

function _do_osx_32 {
    _build "-buildOSXPlayer" "Build/OSX32" ".app"
    _gzip "osx-32" "Build/OSX32"
}

function _do_osx_64 {
    _build "-buildOSX64Player" "Build/OSX64" ".app"
    _gzip "osx-64" "Build/OSX64"
}

function _do_windows_32 {
    _build "-buildWindowsPlayer" "Build/Windows32" ".exe"
    _zip "windows-32" "Build/Windows32"
}

function _do_windows_64 {
    _build "-buildWindows64Player" "Build/Windows64" ".exe"
    _zip "windows-64" "Build/Windows64"
}

case "$1" in
    all)
        _do_linux_32
        _do_linux_64
        _do_osx_32
        _do_osx_64
        _do_windows_32
        _do_windows_64
        ;;
    linux-32)
        _do_linux_32
        ;;
    linux-64)
        _do_linux_64
        ;;
    osx-32)
        _do_osx_32
        ;;
    osx-64)
        _do_osx_64
        ;;
    windows-32)
        _do_windows_32
        ;;
    windows-64)
        _do_windows_64
        ;;
    *)
        echo $"Usage: $0 {all|linux-32|linux-64|osx-32|osx-64|windows-32|windows-64}"
        exit 1
esac
